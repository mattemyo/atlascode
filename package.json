{
    "name": "atlascode",
    "displayName": "Atlassian for VSCode (Official)",
    "description": "Bringing the power of Jira Cloud and Bitbucket Cloud to VSCode - With Atlassian for VSCode you can create and view issues, start work on issues, create pull requests, do code reviews, start builds, get build statuses and more!",
    "keywords": [
        "bitbucket",
        "jira",
        "atlassian",
        "issues",
        "builds",
        "pull requests"
    ],
    "version": "0.0.0",
    "publisher": "atlassian",
    "license": "SEE LICENSE IN LICENSE",
    "homepage": "https://bitbucket.org/atlassianlabs/atlascode/src/master/README.md",
    "repository": {
        "type": "git",
        "url": "https://bitbucket.org/atlassianlabs/atlascode.git"
    },
    "bugs": {
        "url": "https://bitbucket.org/atlassianlabs/atlascode/issues",
        "email": "atlascode@atlassian.com"
    },
    "icon": "images/atlascode-icon.png",
    "preview": false,
    "enableProposedApi": true,
    "galleryBanner": {
        "color": "#c5ddff",
        "theme": "light"
    },
    "engines": {
        "vscode": "^1.33.0"
    },
    "categories": [
        "Other"
    ],
    "activationEvents": [
        "*"
    ],
    "extensionDependencies": [
        "vscode.git",
        "redhat.vscode-yaml"
    ],
    "main": "./build/extension/extension",
    "contributes": {
        "commands": [
            {
                "command": "atlascode.bb.authenticate",
                "title": "Authenticate With Bitbucket",
                "category": "Atlassian"
            },
            {
                "command": "atlascode.bb.clearAuth",
                "title": "Logout From Bitbucket",
                "category": "Atlassian"
            },
            {
                "command": "atlascode.jira.selectProject",
                "title": "Select Working Jira Project",
                "icon": "resources/atlassian-icon.svg",
                "category": "Atlassian"
            },
            {
                "command": "atlascode.jira.selectSite",
                "title": "Select Working Jira Site",
                "icon": "resources/atlassian-icon.svg",
                "category": "Atlassian"
            },
            {
                "command": "atlascode.jira.createIssue",
                "title": "Create New Jira Issue",
                "icon": {
                    "dark": "resources/dark/add.svg",
                    "light": "resources/light/add.svg"
                },
                "category": "Atlassian"
            },
            {
                "command": "atlascode.jira.refreshExplorer",
                "title": "Refresh",
                "icon": {
                    "dark": "resources/dark/refresh.svg",
                    "light": "resources/light/refresh.svg"
                },
                "category": "Atlassian"
            },
            {
                "command": "atlascode.jira.authenticate",
                "title": "Authenticate With Jira",
                "category": "Atlassian"
            },
            {
                "command": "atlascode.jira.clearAuth",
                "title": "Logout from Jira",
                "category": "Atlassian"
            },
            {
                "command": "atlascode.jira.showIssue",
                "title": "Open Jira Issue",
                "category": "Atlassian"
            },
            {
                "command": "atlascode.jira.transitionIssue",
                "title": "Transition Issue",
                "category": "Atlassian"
            },
            {
                "command": "atlascode.jira.assignIssueToMe",
                "title": "Assign Issue to Me",
                "category": "Atlassian"
            },
            {
                "command": "atlascode.jira.startWorkOnIssue",
                "title": "Start work on Jira issue",
                "category": "Atlassian"
            },
            {
                "command": "atlascode.bb.refreshPullRequests",
                "title": "Refresh Bitbucket pull requests",
                "icon": {
                    "dark": "resources/dark/refresh.svg",
                    "light": "resources/light/refresh.svg"
                },
                "category": "Atlassian"
            },
            {
                "command": "atlascode.viewInWebBrowser",
                "title": "View in Browser",
                "category": "Atlassian"
            },
            {
                "command": "atlascode.showConfigPage",
                "title": "Open Settings",
                "category": "Atlassian"
            },
            {
                "command": "atlascode.showWelcomePage",
                "title": "Open Welcome",
                "category": "Atlassian"
            },
            {
                "command": "atlascode.bb.openInBitbucket",
                "title": "Open in Bitbucket",
                "category": "Atlassian"
            },
            {
                "command": "atlascode.bb.openChangeset",
                "title": "Open Changeset",
                "category": "Atlassian"
            },
            {
                "command": "atlascode.bb.createPullRequest",
                "title": "Create Pull Request",
                "icon": {
                    "dark": "resources/dark/add.svg",
                    "light": "resources/light/add.svg"
                },
                "category": "Atlassian"
            },
            {
                "command": "atlascode.bb.viewPullRequest",
                "title": "View Pull Request",
                "category": "Atlassian"
            },
            {
                "command": "atlascode.bb.copyPullRequest",
                "title": "Copy Pull Request Url to Clipboard",
                "category": "Atlassian"
            },
            {
                "command": "atlascode.bb.startPipeline",
                "title": "Start Pipeline",
                "category": "Atlassian"
            },
            {
                "command": "atlascode.bb.refreshPipelines",
                "title": "Refresh Pipelines",
                "icon": {
                    "dark": "resources/dark/refresh.svg",
                    "light": "resources/light/refresh.svg"
                },
                "category": "Atlassian"
            },
            {
                "command": "atlascode.bb.createIssue",
                "title": "Create Bitbucket Issue",
                "icon": {
                    "dark": "resources/dark/add.svg",
                    "light": "resources/light/add.svg"
                },
                "category": "Atlassian"
            },
            {
                "command": "atlascode.bb.refreshIssues",
                "title": "Refresh Bitbucket Issues",
                "icon": {
                    "dark": "resources/dark/refresh.svg",
                    "light": "resources/light/refresh.svg"
                },
                "category": "Atlassian"
            }
        ],
        "viewsContainers": {
            "activitybar": [
                {
                    "id": "atlascode-drawer",
                    "title": "Atlassian",
                    "icon": "resources/atlassian-icon.svg"
                }
            ]
        },
        "views": {
            "atlascode-drawer": [
                {
                    "id": "atlascode.views.jira.assignedIssues",
                    "name": "Your Issues",
                    "when": "atlascode:jiraExplorerEnabled && atlascode:assignedIssuesTreeEnabled"
                },
                {
                    "id": "atlascode.views.jira.openIssues",
                    "name": "Open Issues",
                    "when": "atlascode:jiraExplorerEnabled && atlascode:openIssuesTreeEnabled"
                },
                {
                    "id": "atlascode.views.jira.customJql",
                    "name": "Custom JQL",
                    "when": "atlascode:jiraExplorerEnabled && atlascode:customJQLExplorerEnabled"
                },
                {
                    "id": "atlascode.views.bb.pullrequestsTreeView",
                    "name": "Bitbucket pull requests",
                    "when": "atlascode:bitbucketExplorerEnabled"
                },
                {
                    "id": "atlascode.views.bb.pipelinesTreeView",
                    "name": "Bitbucket pipelines",
                    "when": "atlascode:pipelineExplorerEnabled"
                },
                {
                    "id": "atlascode.views.bb.issuesTreeView",
                    "name": "Bitbucket issues",
                    "when": "atlascode:bitbucketIssuesExplorerEnabled"
                }
            ]
        },
        "menus": {
            "editor/context": [
                {
                    "command": "atlascode.jira.createIssue",
                    "group": "atlascode",
                    "when": "config.atlascode.bitbucket.contextMenus.enabled"
                },
                {
                    "command": "atlascode.bb.createPullRequest",
                    "group": "atlascode",
                    "when": "config.atlascode.bitbucket.contextMenus.enabled"
                },
                {
                    "command": "atlascode.bb.openInBitbucket",
                    "group": "atlascode",
                    "when": "config.atlascode.bitbucket.contextMenus.enabled"
                },
                {
                    "command": "atlascode.bb.openChangeset",
                    "group": "atlascode",
                    "when": "config.atlascode.bitbucket.contextMenus.enabled"
                },
                {
                    "command": "atlascode.bb.viewPullRequest",
                    "group": "atlascode",
                    "when": "config.atlascode.bitbucket.contextMenus.enabled"
                },
                {
                    "command": "atlascode.bb.copyPullRequest",
                    "group": "atlascode",
                    "when": "config.atlascode.bitbucket.contextMenus.enabled"
                },
                {
                    "command": "atlascode.bb.startPipeline",
                    "group": "atlascode",
                    "when": "config.atlascode.bitbucket.pipelines.explorerEnabled"
                }
            ],
            "view/title": [
                {
                    "command": "atlascode.bb.refreshPullRequests",
                    "when": "atlascode:isBBAuthenticated && atlascode:bitbucketExplorerEnabled && view == atlascode.views.bb.pullrequestsTreeView",
                    "group": "navigation"
                },
                {
                    "command": "atlascode.bb.createPullRequest",
                    "when": "atlascode:isBBAuthenticated && atlascode:bitbucketExplorerEnabled && view == atlascode.views.bb.pullrequestsTreeView",
                    "group": "navigation"
                },
                {
                    "command": "atlascode.jira.selectProject",
                    "when": "view =~ /(atlascode.views.jira.assignedIssues|atlascode.views.jira.openIssues)/ && atlascode:isJiraAuthenticated"
                },
                {
                    "command": "atlascode.jira.createIssue",
                    "when": "view =~ /(atlascode.views.jira.assignedIssues|atlascode.views.jira.openIssues)/ && atlascode:isJiraAuthenticated",
                    "group": "navigation"
                },
                {
                    "command": "atlascode.jira.selectSite",
                    "when": "view =~ /(atlascode.views.jira.assignedIssues|atlascode.views.jira.openIssues)/ && atlascode:isJiraAuthenticated"
                },
                {
                    "command": "atlascode.jira.refreshExplorer",
                    "when": "view =~ /(atlascode.views.jira.assignedIssues|atlascode.views.jira.openIssues|atlascode.views.jira.customJql)/ && atlascode:isJiraAuthenticated",
                    "group": "navigation"
                },
                {
                    "command": "atlascode.bb.refreshPipelines",
                    "when": "view =~ /(atlascode.views.bb.pipelinesTreeView)/ && atlascode:isBBAuthenticated",
                    "group": "navigation"
                },
                {
                    "command": "atlascode.bb.createIssue",
                    "when": "atlascode:isBBAuthenticated && view == atlascode.views.bb.issuesTreeView",
                    "group": "navigation"
                },
                {
                    "command": "atlascode.bb.refreshIssues",
                    "when": "view == atlascode.views.bb.issuesTreeView && atlascode:isBBAuthenticated",
                    "group": "navigation"
                }
            ],
            "view/item/context": [
                {
                    "command": "atlascode.jira.transitionIssue",
                    "when": "viewItem == jiraIssue && atlascode:isJiraAuthenticated",
                    "group": "jiraContextMenuGroup1"
                },
                {
                    "command": "atlascode.jira.assignIssueToMe",
                    "when": "viewItem == jiraIssue && atlascode:isJiraAuthenticated",
                    "group": "jiraContextMenuGroup1"
                },
                {
                    "command": "atlascode.jira.startWorkOnIssue",
                    "when": "viewItem == jiraIssue && atlascode:isJiraAuthenticated",
                    "group": "jiraContextMenuGroup1"
                },
                {
                    "command": "atlascode.viewInWebBrowser",
                    "when": "viewItem == jiraIssue && atlascode:isJiraAuthenticated",
                    "group": "jiraContextMenuGroup1"
                },
                {
                    "command": "atlascode.jira.createIssue",
                    "when": "viewItem == jiraIssue && atlascode:isJiraAuthenticated",
                    "group": "jiraContextMenuGroup2"
                },
                {
                    "command": "atlascode.viewInWebBrowser",
                    "when": "viewItem =~ /(pullrequest|pipelineBuild|bitbucketIssue|pipelineBranch)/"
                }
            ],
            "commandPalette": [
                {
                    "command": "atlascode.bb.refreshPullRequests",
                    "when": "false"
                },
                {
                    "command": "atlascode.bb.viewPullRequest",
                    "when": "false"
                },
                {
                    "command": "atlascode.bb.copyPullRequest",
                    "when": "false"
                },
                {
                    "command": "atlascode.bb.startPipeline",
                    "when": "false"
                },
                {
                    "command": "atlascode.bb.refreshIssues",
                    "when": "false"
                },
                {
                    "command": "atlascode.jira.assignIssueToMe",
                    "when": "false"
                },
                {
                    "command": "atlascode.jira.startWorkOnIssue",
                    "when": "false"
                },
                {
                    "command": "atlascode.jira.showIssue",
                    "when": "atlascode:isJiraAuthenticated"
                },
                {
                    "command": "atlascode.jira.selectProject",
                    "when": "atlascode:isJiraAuthenticated"
                },
                {
                    "command": "atlascode.jira.selectSite",
                    "when": "atlascode:isJiraAuthenticated"
                },
                {
                    "command": "atlascode.jira.authenticate",
                    "when": "!atlascode:isJiraAuthenticated && !atlascode:isJiraStagingAuthenticated"
                },
                {
                    "command": "atlascode.bb.authenticate",
                    "when": "!atlascode:isBBAuthenticated"
                },
                {
                    "command": "atlascode.jira.clearAuth",
                    "when": "atlascode:isJiraAuthenticated"
                },
                {
                    "command": "atlascode.bb.clearAuth",
                    "when": "atlascode:isBBAuthenticated"
                }
            ]
        },
        "configuration": {
            "type": "object",
            "title": "Atlassian",
            "properties": {
                "atlascode.outputLevel": {
                    "type": "string",
                    "default": "silent",
                    "enum": [
                        "silent",
                        "errors",
                        "info",
                        "debug"
                    ],
                    "description": "Specifies how much (if any) output will be sent to the Atlassian output channel",
                    "scope": "window"
                },
                "atlascode.enableCharles": {
                    "type": "boolean",
                    "default": false,
                    "description": "Enables proxying requests through the Charles Web Debugger when debugging",
                    "scope": "window"
                },
                "atlascode.offlineMode": {
                    "type": "boolean",
                    "default": false,
                    "description": "puts atlasscode into offline mode if true",
                    "scope": "window"
                },
                "atlascode.showWelcomeOnInstall": {
                    "type": "boolean",
                    "default": true,
                    "description": "Enables showing the welcome screen when a new version is installed",
                    "scope": "window"
                },
                "atlascode.jira.explorer.enabled": {
                    "type": "boolean",
                    "default": true,
                    "description": "Enables the Jira Issue Explorer",
                    "scope": "window"
                },
                "atlascode.jira.explorer.showOpenIssues": {
                    "type": "boolean",
                    "default": true,
                    "description": "Enables the Open Issues in the Jira Issue Explorer",
                    "scope": "window"
                },
                "atlascode.jira.explorer.showAssignedIssues": {
                    "type": "boolean",
                    "default": true,
                    "description": "Enables the Assigned Issues in the Jira Issue Explorer",
                    "scope": "window"
                },
                "atlascode.jira.explorer.refreshInterval": {
                    "type": "integer",
                    "default": 5,
                    "description": "Time (in minutes) to wait before refreshing the Jira Issue Explorer (set to 0 to disable auto-refresh)",
                    "scope": "window"
                },
                "atlascode.jira.statusbar.enabled": {
                    "type": "boolean",
                    "default": true,
                    "description": "Enables the Jira StatusBar Item",
                    "scope": "window"
                },
                "atlascode.jira.statusbar.showProduct": {
                    "type": "boolean",
                    "default": true,
                    "description": "Shows the product name in the status bar",
                    "scope": "window"
                },
                "atlascode.jira.statusbar.showUser": {
                    "type": "boolean",
                    "default": true,
                    "description": "Shows the user's name in the status bar",
                    "scope": "window"
                },
                "atlascode.jira.statusbar.showSite": {
                    "type": "boolean",
                    "default": false,
                    "description": "Shows the default site name in the status bar",
                    "scope": "window"
                },
                "atlascode.jira.statusbar.showProject": {
                    "type": "boolean",
                    "default": false,
                    "description": "Shows the default project name in the status bar",
                    "scope": "window"
                },
                "atlascode.jira.statusbar.showLogin": {
                    "type": "boolean",
                    "default": true,
                    "description": "Shows a login button in the status bar",
                    "scope": "window"
                },
                "atlascode.jira.workingProject": {
                    "type": "object",
                    "properties": {
                        "name": {
                            "type": "string",
                            "default": ""
                        },
                        "id": {
                            "type": "string",
                            "default": ""
                        },
                        "key": {
                            "type": "string",
                            "default": ""
                        }
                    },
                    "description": "Jira project for workspace",
                    "scope": "resource"
                },
                "atlascode.jira.workingSite": {
                    "type": "object",
                    "properties": {
                        "name": {
                            "type": "string",
                            "default": ""
                        },
                        "id": {
                            "type": "string",
                            "default": ""
                        },
                        "scopes": {
                            "type": "array",
                            "items": {
                                "type": "string"
                            },
                            "uniqueItems": true
                        },
                        "avatarUrl": {
                            "type": "string",
                            "default": ""
                        },
                        "baseUrlSuffix": {
                            "type": "string",
                            "default": "atlassian.net"
                        }
                    },
                    "description": "Jira site for workspace",
                    "scope": "resource"
                },
                "atlascode.jira.customJql": {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                            "siteId": {
                                "type": "string",
                                "default": ""
                            },
                            "jql": {
                                "type": "array",
                                "items": {
                                    "type": "object",
                                    "properties": {
                                        "id": {
                                            "type": "string",
                                            "default": ""
                                        },
                                        "enabled": {
                                            "type": "boolean",
                                            "default": true
                                        },
                                        "name": {
                                            "type": "string",
                                            "default": ""
                                        },
                                        "query": {
                                            "type": "string",
                                            "default": ""
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "atlascode.jira.hover.enabled": {
                    "type": "boolean",
                    "default": true,
                    "description": "Enables the Jira hover provider",
                    "scope": "window"
                },
                "atlascode.jira.todoIssues.enabled": {
                    "type": "boolean",
                    "default": true,
                    "description": "Enables the create of Jira issues from TODO comments and similar",
                    "scope": "window"
                },
                "atlascode.jira.todoIssues.triggers": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    },
                    "default": [
                        "TODO:",
                        "BUG:",
                        "FIXME:",
                        "ISSUE:"
                    ],
                    "description": "Strings that will trigger the Jira issue create Code Lens",
                    "scope": "window"
                },
                "atlascode.bitbucket.explorer.enabled": {
                    "type": "boolean",
                    "default": true,
                    "description": "Enables the Bitbucket Pull Request Explorer",
                    "scope": "window"
                },
                "atlascode.bitbucket.pipelines.explorerEnabled": {
                    "type": "boolean",
                    "default": true,
                    "description": "Enables the Bitbucket Pipelines Explorer",
                    "scope": "window"
                },
                "atlascode.bitbucket.pipelines.monitorEnabled": {
                    "type": "boolean",
                    "default": true,
                    "description": "Watch Bitbucket Pipelines For New Build Statuses",
                    "scope": "window"
                },
                "atlascode.bitbucket.pipelines.refreshInterval": {
                    "type": "integer",
                    "default": 5,
                    "description": "Time (in minutes) to wait before refreshing the Pipelines status",
                    "scope": "window"
                },
                "atlascode.bitbucket.issues.explorerEnabled": {
                    "type": "boolean",
                    "default": true,
                    "description": "Enables the Bitbucket Issues Explorer",
                    "scope": "window"
                },
                "atlascode.bitbucket.issues.createJiraEnabled": {
                    "type": "boolean",
                    "default": false,
                    "description": "Shows a create jira issue button on the Bitbucket issue screen",
                    "scope": "window"
                },
                "atlascode.bitbucket.issues.monitorEnabled": {
                    "type": "boolean",
                    "default": true,
                    "description": "Watch for new Bitbucket Issues",
                    "scope": "window"
                },
                "atlascode.bitbucket.issues.refreshInterval": {
                    "type": "integer",
                    "default": 15,
                    "description": "Time (in minutes) to wait before refreshing the Bitbucket Issues",
                    "scope": "window"
                },
                "atlascode.bitbucket.explorer.refreshInterval": {
                    "type": "integer",
                    "default": 5,
                    "description": "Time (in minutes) to wait before refreshing the pull reqeusts explorer (set to 0 to disable auto-refresh)",
                    "scope": "window"
                },
                "atlascode.bitbucket.explorer.relatedJiraIssues.enabled": {
                    "type": "boolean",
                    "default": true,
                    "description": "Show related Jira issues for Bitbucket pull requests",
                    "scope": "window"
                },
                "atlascode.bitbucket.explorer.relatedBitbucketIssues.enabled": {
                    "type": "boolean",
                    "default": true,
                    "description": "Show related Bitbucket issues for Bitbucket pull requests",
                    "scope": "window"
                },
                "atlascode.bitbucket.explorer.notifications.pullRequestCreated": {
                    "type": "boolean",
                    "default": true,
                    "description": "Periodically check for newly created pull reqeuests and show an information message if there are any",
                    "scope": "window"
                },
                "atlascode.bitbucket.contextMenus.enabled": {
                    "type": "boolean",
                    "default": true,
                    "description": "Enables Bitbucket context menu items in editor",
                    "scope": "window"
                },
                "atlascode.bitbucket.statusbar.enabled": {
                    "type": "boolean",
                    "default": true,
                    "description": "Enables the Bitbucket StatusBar Item",
                    "scope": "window"
                },
                "atlascode.bitbucket.statusbar.showProduct": {
                    "type": "boolean",
                    "default": true,
                    "description": "Shows the product name in the status bar",
                    "scope": "window"
                },
                "atlascode.bitbucket.statusbar.showUser": {
                    "type": "boolean",
                    "default": true,
                    "description": "Shows the user's name in the status bar",
                    "scope": "window"
                },
                "atlascode.bitbucket.statusbar.showLogin": {
                    "type": "boolean",
                    "default": true,
                    "description": "Shows a login button in the status bar",
                    "scope": "window"
                }
            }
        }
    },
    "scripts": {
        "vscode:uninstall": "node ./build/extension/uninstall.js",
        "vscode:prepublish": "react-scripts-ts build && webpack --env.production",
        "compile": "react-scripts-ts build && webpack --env.development",
        "watch": "tsc --watch -p ./",
        "postinstall": "node ./node_modules/vscode/bin/install && patch-package",
        "test": "yarn run compile && node ./node_modules/vscode/bin/test"
    },
    "dependencies": {
        "@atlaskit/avatar": "^14.1.2",
        "@atlaskit/avatar-group": "^2.1.5",
        "@atlaskit/breadcrumbs": "^6.0.10",
        "@atlaskit/button": "^10.1.1",
        "@atlaskit/checkbox": "^5.0.5",
        "@atlaskit/comment": "6.0.29",
        "@atlaskit/css-reset": "^3.0.2",
        "@atlaskit/datetime-picker": "^6.3.24",
        "@atlaskit/droplist": "^7.0.16",
        "@atlaskit/field-base": "^11.0.12",
        "@atlaskit/form": "^5.1.3",
        "@atlaskit/icon": "^16.0.5",
        "@atlaskit/icon-object": "^3.0.5",
        "@atlaskit/icon-priority": "^3.0.2",
        "@atlaskit/inline-dialog": "^9.0.13",
        "@atlaskit/input": "^4.0.9",
        "@atlaskit/layer-manager": "^5.0.18",
        "@atlaskit/logo": "^9.2.5",
        "@atlaskit/lozenge": "^6.2.2",
        "@atlaskit/modal-dialog": "^7.1.1",
        "@atlaskit/page": "^8.0.8",
        "@atlaskit/page-header": "^6.0.7",
        "@atlaskit/panel": "^0.0.10",
        "@atlaskit/radio": "^0.4.6",
        "@atlaskit/reduced-ui-pack": "^10.1.1",
        "@atlaskit/section-message": "^1.0.15",
        "@atlaskit/select": "^6.1.15",
        "@atlaskit/size-detector": "^5.0.8",
        "@atlaskit/spinner": "^9.0.13",
        "@atlaskit/table-tree": "^5.0.4",
        "@atlaskit/tag": "^6.1.0",
        "@atlaskit/tag-group": "^6.0.8",
        "@atlaskit/tooltip": "^12.1.9",
        "@atlaskit/util-shared-styles": "^3.0.0",
        "@atlassian/jira": "^0.1.0",
        "@deviniti/jql-autocomplete": "^1.0.2",
        "ajv": "^6.5.4",
        "ajv-keywords": "^3.2.0",
        "analytics-node": "^3.3.0",
        "bitbucket": "^1.14.0",
        "escape-string-regexp": "^1.0.5",
        "express": "^4.16.3",
        "git-url-parse": "^11.1.2",
        "handlebars": "4.0.12",
        "is-online": "^8.1.0",
        "jquery": "^3.3.1",
        "keytar": "^4.3.0",
        "lodash.debounce": "^4.0.8",
        "macaddress": "^0.2.9",
        "moment": "^2.23.0",
        "passport": "^0.4.0",
        "passport-atlassian-oauth2": "^1.0.0",
        "passport-bitbucket-oauth2": "^0.1.2",
        "passport-oauth2": "^1.4.0",
        "passport-oauth2-refresh": "^1.1.0",
        "react": "^16.3.2",
        "react-dom": "^16.3.2",
        "react-final-form": "^4.0.2",
        "react-intl": "^2.8.0",
        "react-loadable": "^5.5.0",
        "semver": "^5.6.0",
        "spawn-sync": "^2.0.0",
        "styled-components": "^3.2.6",
        "tunnel": "^0.0.6",
        "turndown": "^5.0.3",
        "uuid": "^3.3.2"
    },
    "devDependencies": {
        "@types/analytics-node": "^3.1.1",
        "@types/atlaskit__button": "^6.4.1",
        "@types/escape-string-regexp": "^1.0.0",
        "@types/git-url-parse": "^9.0.0",
        "@types/handlebars": "4.0.39",
        "@types/jquery": "^3.3.23",
        "@types/keytar": "^4.0.1",
        "@types/lodash.debounce": "^4.0.4",
        "@types/mocha": "^5.2.2",
        "@types/moment": "^2.13.0",
        "@types/node": "^8.10.25",
        "@types/node-fetch": "^2.1.4",
        "@types/passport": "^0.4.6",
        "@types/passport-oauth2": "^1.4.6",
        "@types/react": "^16.3.2",
        "@types/react-dom": "^16.0.5",
        "@types/react-loadable": "^5.4.1",
        "@types/semver": "^5.5.0",
        "@types/turndown": "^5.0.0",
        "@types/uuid": "^3.4.4",
        "mocha": "^5.2.0",
        "patch-package": "^6.1.2",
        "postinstall-postinstall": "^2.0.0",
        "react-scripts-ts": "^3.1.0",
        "ts-loader": "^5.3.1",
        "tslint": "^5.11.0",
        "typescript": "^3.2.2",
        "vscode": "^1.1.26",
        "webpack": "^4.26.1",
        "webpack-cli": "^3.1.2"
    }
}