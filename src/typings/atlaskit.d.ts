declare module '@atlaskit/page';
declare module '@atlaskit/page-header';
declare module '@atlaskit/table-tree';
declare module '@atlaskit/avatar';
declare module '@atlaskit/avatar-group';
declare module '@atlaskit/checkbox';
declare module '@atlaskit/comment';
declare module '@atlaskit/breadcrumbs';
declare module '@atlaskit/lozenge';
declare module '@atlaskit/tag';
declare module '@atlaskit/tag-group';
declare module '@atlaskit/tooltip';
declare module '@atlaskit/theme';
declare module '@atlaskit/size-detector';
declare module '@atlaskit/panel';

/* BEGIN - ONLY USE FOR JQL EDITING */
declare module '@atlaskit/field-base';
declare module '@atlaskit/input';
declare module '@atlaskit/droplist';
/* END - ONLY USE FOR JQL EDITING */

declare module '@atlaskit/select';
declare module '@atlaskit/spinner';
declare module '@atlaskit/modal-dialog';
declare module '@atlaskit/inline-dialog';
declare module '@atlaskit/section-message';
declare module '@atlaskit/logo/dist/esm/BitbucketLogo/Icon';
declare module '@atlaskit/logo/dist/esm/JiraLogo/Icon';
declare module '@atlaskit/logo/dist/esm/ConfluenceLogo/Icon';
declare module '@atlaskit/form';
declare module '@atlaskit/datetime-picker';
declare module '@atlaskit/radio';
